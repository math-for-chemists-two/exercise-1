# Python-Übung 1 - Komplexe Zahlen

Die 1. Python-Übung der Vorlesung Mathematik für Studierende der Chemie II.

## Themen des Übungszettels
* Logarithmengesetze
* Komplexe Zahlen
* Wiederholung von Ableitung und Integration

## Aufbau der Python-Übung

Die erste Python-Übung wird hauptsächlich darauf verwendet, JupyterHub und Python kennenzulernen.
Wir müssen davon ausgehen, dass die Übungen für die Studierenden der erste Berührungspunkt mit
diesen Tools ist. 

1. Auf dem Übungszettel gibt es eine ausführliche Erklärung der Anmeldung im JupyterHub und
des Downloads dieses Repositories.
**_⏰ ca. 10 min_**
2. Kurze Einführung in die Benutzeroberfläche von JupyterHub. **_⏰ ca. 5 min_**
3. Speichern von Variablen und Addieren von Zahlen am Beispiel der beiden komplexen Zahlen aus 
   Aufgabe 2 des Übungszettels. **_⏰ ca. 10 min_**

### Vermittelte Python-Konzepte

* Nutzung von JupyterHub.
* Ausführen von Code
* Variablendefinition und einfache Operatoren wie +, -, *, /

## Nutzung

1. Anmeldung auf [jupyter-cloud](https://jupyter-cloud.gwdg.de) mit den
   studentischen Zugangsdaten (Stud.IP, FlexNow, ...) oder dem GWDG-Account.
2. In der oberen Menüleiste: Reiter **Git** 👉🏿 **Clone a Repository**.
3. Es öffnet sich ein Popup-Dialog. Hier den HTTPS-Clone-Link dieses Projekts eintragen: 
   [https://gitlab.gwdg.de/math-for-chemists-two/exercise-1.git](https://gitlab.gwdg.de/math-for-chemists-two/exercise-1.git)
4. Im neuen Ordner auf das Jupyter-Notebook navigieren.
5. Fertig!

## Autoren

* Alexander Knoll - @aknoll - [aknoll@gwdg.de](mailto:aknoll@gwdg.de)
